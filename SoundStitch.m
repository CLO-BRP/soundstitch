% script SoundStitch
%
%   Multiplex single-channel files, sliding to line time stamp
%
%   TO DO

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
% user input
inpath = 'E:\Bitbucket_GIT\SoundStitch\data\dz_trailArray';
outpath = 'E:\Bitbucket_GIT\SoundStitch\data\dz_trailArray_NEWstitch';
outLabel = 'dz_trailArray';

% list sound files
tic
h = waitbar(0,'Reading sound file headers');
infile = fastDir(inpath,true,'wav');
[p,f] = cellfun(@fileparts,infile,'Unif',0);
[p2,chan] = cellfun(@fileparts,p,'Unif',0);

% read headers of audio files
BAD = false(size(f,1),1);
badFN = cell(0);
try
    info = cellfun(@audioinfo,infile);
catch
    %If there are one or more corrupt sound files, remove them from processing
    info = struct(audioinfo(infile{1}));
    for i = 2:size(f,1)
        try
            info(i) = audioinfo(infile{i});
            if isequal(info(i).TotalSamples,0)
                BAD(i) = true;
                badFN = [badFN;infile{i}];
            end
        catch
            BAD(i) = true;
            badFN = [badFN;infile{i}];
% %             disp(['BAD: ',infile{i}]) %skip and display any sound files that MATLAB cannot read
        end
    end
end

% % % add audio files with no samples to BAD list (move to audioinfo try-catch block
totalSamples = [info.TotalSamples];
% % emptyFiles = totalSamples'==0;
% % if any(emptyFiles)
% %     BAD = BAD | emptyFiles;
% %     badFN = [badFN;infile(emptyFiles)];
% % end

% remove bad files
if any(BAD)
    fprintf(1,'\nNumber of bad sound files = %.0f\n\n',sum(BAD));
    fprintf(1,'Names of bad sound files:\n\n');
    disp(badFN);
    fnFull = fullfile(outpath,'badSoundFiles.txt');
    fid = fopen(fnFull,'w+');
    fprintf(fid,'Number of bad sound files = %.0f\n\n',sum(BAD));
    fprintf(fid,'Names of bad sound files:\n');
    fprintf(fid,'%s\n',badFN{:});
    fclose(fid);
    infile = infile(~BAD);
    f = f(~BAD);
    chan = chan(~BAD);
    info = info(~BAD);
% %     totalSamples = totalSamples(~BAD);
    totalSamples = [info.TotalSamples];
end

Fs = [info.SampleRate];
assert(all(Fs == Fs(1)), 'Sample rate of sound files must be the same')
Fs = Fs(1);
dur = [info.Duration];
nbits = [info.BitsPerSample];
assert(all(nbits == nbits(1)), 'Bit depth of sound files must be the same')
nChannels = [info.NumChannels];
assert(all(nChannels == 1), 'Number of channels in sound files must be the same')

% read time stamps
mask = '\d\d\d\d\d\d\d\dT\d\d\d\d\d\d\d\d\d';
tstr = regexp(f,mask,'match');
Cbegin = cellfun(@(x) x{1},tstr,'Unif',0);
Cend = cellfun(@(x) x{2},tstr,'Unif',0);
inFormat = 'yyyymmddTHHMMSSFFF';
tBegin = datenum(Cbegin,inFormat);
tEnd = datenum(Cend,inFormat);

% Calculate duration by times stamps and by counting samples
durStamp = tEnd - tBegin;
durStampSec = durStamp.*86400;
totalSamplesStamp = round(durStampSec.*Fs);
durStampStr = datestr(durStamp,'HH:MM:SS.FFF');
durSampleSec = totalSamples'./Fs;
durSample = durSampleSec./86400;
durSampleStr = datestr(durSample,'HH:MM:SS.FFF');
tdiff = diff([durStampSec,durSampleSec],1,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen(fullfile(outpath,'dz_2020_array_stretch.csv'),'w+');
% % fprintf(fid,'Duration (s)\n\n');
fprintf(fid,'File Name,Duration by Time Stamps (s),Duration by Number of Samples (s),Diff (s)\n');
for i = 1:size(durStampStr,1)
% %     fprintf(fid,'%s,%s,%s,%.3f\n',f{i},durStampStr(i,:),durSampleStr(i,:),tdiff(i));
    fprintf(fid,'%s,%f,%f,%f\n',f{i},durStampSec(i,:),durSampleSec(i,:),tdiff(i));
end
fclose(fid);
% % keyboard;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
durTest = [durStampSec,durSampleSec,tdiff];

%---
% Make hour index
%---
tMin = min(tBegin); %begin time of earliest input file
tMax = max(tEnd); %send time of latest input file
% % numSamples = round((tMax - tMin) .* 86400,3) .* Fs;
t0 = floor(tMin*24)/24; %begin time of first hour file output
t1 = ceil(tMax*24)/24 - 1; %begin time of last hour file output
beginHrIDX = floor(tBegin.*24) - floor(tMin*24) + 1;
endHrIDX = floor(tEnd.*24) - floor(tMin*24) + 1;
numHr = max(endHrIDX);

%---
% Segregate data by channel
%---
chanNames = unique(chan);
numChan = size(chanNames,1);
infileChan = cell(1,numChan);
tBeginChan = cell(1,numChan);
tEndChan = cell(1,numChan);
beginHrIDXchan = cell(1,numChan);
endHrIDXchan = cell(1,numChan);
totalSamplesChan = cell(1,numChan);
totalSamplesStampChan = cell(1,numChan);
for ch = 1:numChan
    idx = strcmp(chan,chanNames{ch});
    infileChan{ch} = infile(idx);
    tBeginChan{ch} = tBegin(idx);
    tEndChan{ch} = tEnd(idx);
    beginHrIDXchan{ch} = beginHrIDX(idx);
    endHrIDXchan{ch} = endHrIDX(idx);
    totalSamplesChan{ch} = totalSamples(idx);
    totalSamplesStampChan{ch} = totalSamplesStamp(idx);
end
% % len = cellfun(@length,infileChan);
% % maxLen = max(len);

%---
% Initialize output paths
%---
if ~isfolder(outpath)
    mkdir(outpath)
end

%---
% Assemble and write output sound files
%---
samplesPerHr = 3600 .* Fs;
remainIn = {[]};
remainIn(1:numChan) = remainIn;
for hr = 1:numHr
    
    waitbar(hr/numHr,h,sprintf('Processing hour %.0f of %.0f',hr,numHr));
    
    % Prefill current soundstream with zeros
    xOut = zeros(samplesPerHr,numChan);
        
    %add remaining sample from sound file starting in previous hour
    for ch = 1:numChan
        if ~isempty(remainIn{ch})
            remainIDX = length(remainIn{ch});
            xOut(1:remainIDX,ch) = remainIn{ch};
        end
    end
    
    %reinitialize remainIn
    lastRemainIn = remainIn;
    remainIn = {[]};
    remainIn(1:numChan) = remainIn;
    
    %for each channel
    tBeginHr = t0 + (hr-1)/24;
    for ch = 1:numChan
        
        %for each sound file that overlaps the current hour
        IDX = find(beginHrIDXchan{ch}==hr);%IDX - which sound files overlap current hour
        for i = 1:length(IDX)
            currIDX = IDX(i);
            
            % Read sound file beginning in current hour
try
            xIn = audioread(infileChan{ch}{currIDX});
catch ME
    fprintf(2,'\nERROR reading sound file %s\n',infileChan{ch}{currIDX});
    keyboard;
end
% % fprintf('\nreading sound file in channel %.0f in hour %.0f: %.0f seconds\n',ch,hr,toc);%%%%%%%%%%%%%%%%%%%%%%%%     
            % Add or drop samples to make duration agree with difference in time stamps in file name
            neededSamples = totalSamplesStampChan{ch}(currIDX);
            currSamples = totalSamplesChan{ch}(currIDX);
            diffSamples = currSamples - neededSamples;
            proportion = 1/(abs(diffSamples)+1);
            chopIDX = round(currSamples .* (proportion:proportion:1-proportion))+1;           
            if diffSamples > 0
                xInNew = xIn;
                xInNew(chopIDX) = [];
            elseif diffSamples < 0
                xInNew = zeros(neededSamples,1);
                chopIDX = [0 , chopIDX];
                for j = 2:length(chopIDX)
try
                    xInNew(chopIDX(j-1)+j-1:chopIDX(j)+j-2) = xIn(chopIDX(j-1)+1:chopIDX(j));
                    xInNew(chopIDX(j)+j-1) = xIn(chopIDX(j));
catch ME
    fprintf(2,'\nERROR: xIn stretched beyond end of xInNew segment\n');
    keyboard;
end
                end
                    xInNew(chopIDX(j)+j:end) = xIn(chopIDX(j)+1:end);
            end
% % fprintf('stretching channel %.0f in hour %.0f: %.0f seconds\n',ch,hr,toc);%%%%%%%%%%%%%%%%%%%%%%%%    
            totalSamplesCheck = length(xInNew);
            if ~isequal(totalSamplesCheck,totalSamplesStampChan{ch}(currIDX))
                fprintf(2,'\nIncorrect number of samples after adjustment.\n\n')
                keyboard;
            end 

            % Copy adjusted samples into output with correct file offset
            inStart = 1;
            inStop = min(samplesPerHr-length(lastRemainIn{ch}), min(size(xInNew,1), samplesPerHr-round((tBeginChan{ch}(currIDX)-tBeginHr)*86400*Fs)));
            outStart = round((tBeginChan{ch}(currIDX) - tBeginHr)*86400*Fs) + 1; %this has got to be lastRemainIn(ch)+something
            outStop = round(min(samplesPerHr,totalSamplesStampChan{ch}(currIDX)+outStart-1));
            xOut(outStart:outStop,ch) = xInNew(inStart:inStop);
            
            % Save remaining samples for next hour of output
            remainStart = inStop + 1;
            if remainStart > 0
                remainIn{ch} = xInNew(remainStart:end);
            else
                remainIn{ch} = [];
            end
% % fprintf('inserting samples into sound file: %.0f seconds\n',toc);%%%%%%%%%%%%%%%%%%%%%%%%     
        end
    end
	
    % Write output
    outFile = sprintf('%s_%s.wav', outLabel, datestr(tBeginHr,30));
    outFileFull = fullfile(outpath,outFile);
    audiowrite(outFileFull,xOut,Fs)
% % fprintf('writing sound file: %.0f seconds\n',toc);%%%%%%%%%%%%%%%%%%%%%%%%     

end

fprintf(1,'\n\nProcessing complete in %s\n',datestr(toc/86400,'HH:MM:SS'));
waitbar(1,h,sprintf('Processing complete in %s',datestr(toc/86400,'HH:MM:SS')));
