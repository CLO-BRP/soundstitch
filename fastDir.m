function d = fastDir(p, r, filter)
% 
% List files in current directory and all subdirectories
%
%   Input
%       p - root directory for search
%       r - recursive directory (true/false)
%       filter - file extension to use in filter (character vector)
%
%   Output
%       d - list of files
switch(computer)
    case 'PCWIN64'
        if ~exist('r', 'var')
            r = false;
        end
        if r
            option = '/b/s/a-d | sort';
        else
            option = '/b/a-d | sort';
        end
        if exist("filter", 'var')
            p2 = fullfile(p, sprintf('*.%s', filter));
        else
            p2 = p;
        end
        [status, result] = dos(sprintf('dir "%s" %s', p2, option));
        if status || strcmp(result,sprintf('File Not Found\n'))
            d = {};
            return;
        end
        fn = strsplit(result, newline)';
        fn(strcmp('', fn)) = [];
        if r
            d = fn;
        else
            d = fullfile(p, fn);
        end
    case 'MACI64'
        if exist("filter", 'var')
            p2 = fullfile(p, sprintf('*.%s', filter));
        else
            p2 = p;
        end
        d = dir(p2);
        idx = [d.isdir]';
        fn = {d.name}';
        fn(idx) = [];
        fn(strncmp(fn,'.',1)) = [];
        d = fullfile(p, fn);
    case 'GLNXA64'
        error('"fastDIR" is not compatible with Linux.')
end