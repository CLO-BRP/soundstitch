% script "fileRenameFromDatasheet"
%
%   Make copy of files with new names specified in data file
%
%   Set up Notes
%       Before running the data a "NotOnNAS" field must be added to all the
%       Excel workbooks with renaming data. The values in this field should 
%       be set to 0, except for the lines Peter has highlighted with yellow 
%       background and annotated "not on the NAS" where NotOnNAS should be -1.  

% Initializations
clear
clc
tic
filter = 'wav'; %sound file extension
dataDir = 'E:\Bitbucket_GIT\SoundStitch\data\dz_2020_array_renamingData_2ndDeploy';
inDir = 'E:\Bitbucket_GIT\SoundStitch\data\dz_2020_array';
outDir = 'E:\Bitbucket_GIT\SoundStitch\data\dz_2020_array_RENAME_2ndDeploy';
dataSheet = 'FileNames';
oldNameField = 'OriginalFileName-Cornell';
newNameField = 'TimeFixFIleName-Cornell';
errorField = 'ErrorOutOfBounds'; %"-1" means time correction error is too high
removedField = 'NotOnNAS'; %"-1" means sound file listed in data file has been removed from file system

% Check that output directory is empty
h = waitbar(0,'Starting ...','Name','fileRenameFromDatasheet');
assert(isfolder(outDir),'ERROR: Output directory does not exist.')
assert(isempty(fastDir(outDir,true)),'ERROR: Output directory is not empty.')

% Read XLSX files
waitbar(0.2,h,'Reading XLSX files')
dataFull = fastDir(dataDir,false,'xlsx');
oldName = cell(0);
newName = cell(0);
for i = 1:length(dataFull)
    [num,txt,raw] = xlsread(dataFull{i},dataSheet);
    header = raw(1,:);
    body = raw(2:end,:);
    oldNameCurr = body(:,strcmp(header,oldNameField));
    newNameCurr = body(:,strcmp(header,newNameField));
    
    %Peter says all tables have two "Out Of Bounds" columns, and he wants
    %to use the one in column
% %     errorOutOfBounds = body(:,strcmp(header,errorField));
    errorOutOfBounds = body(:,8);
    if ~isequal(find(strcmp(header,errorField),1,'last'),8)
        fprintf(2,'WARNING: Out of Bounds field is no in column H\n');
        keyboard;
    end
    
    errorOutOfBounds = cell2mat(errorOutOfBounds(:,1));
    removed = body(:,strcmp(header,removedField));
    removed = cell2mat(removed(:,1));    
    assert(~isempty(oldNameCurr),'ERROR: Data file has no old sound file names\n  %s\n',dataFull{i})
    assert(~isempty(newNameCurr),'ERROR: Data file has no new sound file names\n  %s\n',dataFull{i})
    assert(~isempty(errorOutOfBounds),'ERROR: Data file has no %s field\n  %s\n',errorField,dataFull{i})
    assert(~isempty(removed),'ERROR: Data file has no %s field\n  %s\n',removedField,dataFull{i})    
    missingData = ...
        cell2mat(cellfun(@(x) any(isnan(x)),oldNameCurr,'Unif',false)) | ...
        cell2mat(cellfun(@(x) any(isnan(x)),newNameCurr,'Unif',false)) | ...
        isnan(errorOutOfBounds);
    if any(missingData)
        oldNameCurr(missingData) = [];
        newNameCurr(missingData) = [];
        errorOutOfBounds(missingData) = [];
        removed(missingData) = [];
    end

    % Disregard files with too much time recalculation error
    oldNameCurr(errorOutOfBounds==-1 | removed==-1) = [];
    newNameCurr(errorOutOfBounds==-1 | removed==-1) = [];
    
    % Append old name and new name, clear temporary variables
    oldName = [oldName ; oldNameCurr];
    newName = [newName ; newNameCurr];
    clear header body oldNameCurr newNameCurr errorOutOfBounds missingData
end

% Check that all files specified in data file are present
waitbar(0.4,h,'Checking that sound files listed in XLSX files are present')
fFull = fastDir(inDir,true,filter);
[p,f,ext] = cellfun(@fileparts,fFull,'UniformOutput',false);
fext = strcat(f,ext);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kludge so changes in GPS data do not cause file name mismatch
oldNameTrimmed = cellfun(@(x) x(1:end-22),oldName,'Unif',0);
fextTrimmed = cellfun(@(x) x(1:end-22),fext,'Unif',0);
[matchingFiles,idx] = ismember(oldNameTrimmed,fextTrimmed);

% Original kludgeless code
% % [matchingFiles,idx] = ismember(oldName,fext);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

missingFiles = ~matchingFiles;
if any(missingFiles)
    fprintf(2,'\n\nERROR: These files do not exist and cannot be renamed.\n');
    C = oldName(missingFiles)';
    fprintf(2,'   %s\n',C{:});
    fprintf(2,'\nEXECUTION TERMINATED\n');
    return;
end
% % if any(missingFiles)
% %     fprintf(2,'\n\nERROR: These files do not exist and cannot be renamed.\n');
% %     C = oldName(missingFiles)';
% %     fprintf(2,'   %s\n',C{:});
% %     answer = questdlg(...
% %         'Some sound files listed in the XLSX filesare missing. Proceed anyway?',...
% %         'fileRenameFromDatasheet',...
% %         'Yes','No','No');
% %     switch answer
% %         case 'Yes'
% %             skipIDX = idx==0;
% %             idx(skipIDX) = [];
% %             fFull(skipIDX) = [];
% %             p(skipIDX) = [];
% %             newName(missingFiles) = [];
% %         case 'No'
% %             fprintf(2,'\nEXECUTION TERMINATED\n');
% %             return;
% %     end
% % end

% Make copy of files using new names specified in data file
% (Assumes all files specified in data table are present in inDir.)
% (TO DO: preserve subfolders)
waitbar(0.6,h,'Copying sound files and renaming.')
oldNameFull = fFull(idx);
subdir = strrep(p,inDir,'');
outDirFull = fullfile(outDir,subdir(idx));
cellfun(@mkdir,unique(outDirFull))
newNameFull = fullfile(outDirFull,newName);
cmd = cellfun(@(x,y) sprintf('copy "%s" "%s" /Y/Z',x,y),oldNameFull,newNameFull,'Unif',false);
cellfun(@(x) dos(x), cmd);
waitbar(1,h,sprintf('Processing complete in %s\n',datestr(toc/86400,'HH:MM:SS')));

disp(' ')
disp('**********************************************************************')
fprintf('Processing complete in %s\n',datestr(toc/86400,'HH:MM:SS'));
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test results
clear
clc
inDir = 'E:\Bitbucket_GIT\SoundStitch\data\dz_2020_array_TESTinput';
outDir = 'E:\Bitbucket_GIT\SoundStitch\data\dz_2020_array_RENAME';
filter = '*.wav';
f1 = fastDir(inDir,true,filter);
[~,f1] = cellfun(@fileparts,f1,'UniformOutput',false);
f2 = fastDir(outDir,true,filter);
[~,f2] = cellfun(@fileparts,f2,'UniformOutput',false);

for i = 1:length(f2)
    if ~strcmp(f1{i+71},f2{i})
        fprintf('%d\n  %s\n  %s\n\n',i,f1{i+71},f2{i});
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,x] = cellfun(@fileparts,oldNameFull,'Unif',false);
[~,y] = cellfun(@fileparts,newNameFull,'Unif',false);
for i = 1:length(x)
    if ~strcmp(x{i},y{i})
        fprintf('%d\n  %s\n  %s\n',i,x{i},y{i});
    end
end

